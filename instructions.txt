 _____                _     _____           _   _____         _   
|  ___| __ ___  _ __ | |_  | ____|_ __   __| | |_   _|__  ___| |_ 
| |_ | '__/ _ \| '_ \| __| |  _| | '_ \ / _` |   | |/ _ \/ __| __|
|  _|| | | (_) | | | | |_  | |___| | | | (_| |   | |  __/\__ \ |_ 
|_|  |_|  \___/|_| |_|\__| |_____|_| |_|\__,_|   |_|\___||___/\__|
-----------------------------------------------------------------------------------------------------------------
This is a small front-end focused project to determine if candidates have basic skills required to
perform daily tasks on the product development team.

Instructions:
1) Recreate the mock-up found in this directory using the Vue/NUXT framework.
	* If you are unfamiliar with Vue/NUXT visit: https://nuxtjs.org/ for reference

2) The hero will be a rotating slider

3) Use Vue's templating to render out the included comments object in app.json.
	* No changing of the original object is allowed, handle all data malformations and
          formatting with your code (assume that app.json is a response form an external API).
	* pay special attention to things like how the data is ordered and how every element is formatted.

4) The layout must be responsive, how specific elements on the page are rearranged is up to you.

5) Be ready to discuss how you approach your code decisions.

6) We ask that you take no longer than 3-4 hours on this test.
